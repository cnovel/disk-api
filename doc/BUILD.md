#### [介绍文档](../README.md) | 部署文档 

### 准备工作
```
JDK >= 1.8 (推荐1.8版本)
MySQL >= 5.7 (推荐5.7版本)
Maven >= 3.0
```

### 运行系统

* 将下载好的项目导入到idea中。
* 在数据库中创建`disk`数据库。
* 运行`admin`模块下的`DiskApplication.java`（运行前建议对整个项目进行`maven install`）。
* 直至控制台中打出启动系统，并且没有任何异常，则启动成功。

### 必要配置
1. 项目的文件存储根目录，如果是`windows`系统，请根据自己的盘符进行相应的配置。
2. 修改数据库信息。
4. 修改资源存储配置信息`project.resource`。
4. 修改认证服务器配置信息`project.authorization`。

#### mysql:

编辑`resources`目录下的`application-dev.yml`
```yaml
spring:
  datasource:
    druid:
      #数据库连接url
      url: 
      #数据库连接用户名
      username: 
      #数据库连接密码
      password: 
```

#### `resource`文件存储配置:

编辑`resources`目录下的`application-dev.yml`

```yaml
project:
  resource:
    #文件存储根目录
    root-path: E:\\tmp\\
    #个人用户的可用空间大小
    storage-total-size: 10737418240
```


### 部署系统
1. 配置好上面的配置。
2. 在项目根目录下运行`mvn clean package -Dmaven.test.skip=true`命令，等待编译完成。
3. 把`docker`文件夹下的`jar`包放到服务器上运行即可。


### 通过docker容器部署
1. 按照上述文档将程序编译完成。
2. 将`docker`文件夹下`builddocker.sh`、`Dockerfile`和编译好的`jar`文件夹拷贝到`docker`服务器上。
3. 执行

```bash
$ sudo chmod 777 builddocker.sh

$ ./builddocker.sh
```

### 配置项加密说明
1. 在配置文件中加入`jasypt.encryptor.password`配置信息。（`这个密码是用来对配置进行加解密的秘钥，一定妥善保管`）
2. 运行`admin`模块下的`AdminApplicationTests.java`中的测试方法，把`root`字符串替换为自己想要加密的属性配置项。
3. 将得到的加密后的配置项配置到对应的配置上，且用`ENC()`包裹，例如：`ENC(XW2daxuaTftQ+F2iYPQu0g==)`，括号中间的则是配置信息。
4. 配置信息配置完后删除`jasypt.encryptor.password`配置项。
5. 在启动的环境中加入`jasypt.encryptor.password`环境信息，例如在`Dockerfile`文件中启动程序时加入了`"-Djasypt.Encryptor.Password=VLu3H58dxYAsv3TIGOueaXIXBbhbT2"`信息，这里的密码和加密时使用的一致。
### 通过jenkins部署

暂略


### 常见问题
1. 如果使用`Mac`需要修改`application-dev.yml`文件路径`profile`。
2. 如果使用`Linux`提示表不存在，设置大小写敏感配置在`/etc/my.cnf`添加`lower_case_table_names=1`，重启MYSQL服务。
3. 如果提示当前权限不足，无法写入文件请检查`profile`配置目录是否可读可写，或者无法访问此目录。












