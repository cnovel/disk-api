#!/bin/bash
#docker部分

echo '================打包完成，开始制作镜像================'

echo '================停止容器 diskapi================'
sudo docker stop diskapi
echo '================删除容器 diskapi================'
sudo docker rm diskapi
echo '================删除镜像 diskapi:latest================'
sudo docker rmi diskapi:latest
echo '================build 镜像 diskapi:latest================'
sudo docker build -t diskapi:latest  .
echo '================运行容器 diskapi================'
sudo docker run --name=diskapi --restart always -d -p 8883:8888 -v /data:/disk -v /logs:/logs diskapi:latest

echo "finished!"
echo '================部署完成================'
