package com.novel.disk.common;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Base64;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * sdk工具类
 *
 * @author novel
 * @date 2019/7/2
 */
public class ApiUtils {
    public static <T> T[] concat(T[] first, T[] second) {

        T[] result = Arrays.copyOf(first, first.length + second.length);

        System.arraycopy(second, 0, result, first.length, second.length);

        return result;

    }

    public static int findFirstNonNumAndLetter(String str) {
        for (int i = 0; i < str.length(); ++i) {
            int chr = str.charAt(i);
            if (!isNum(chr) && !isLetter(chr)) {
                return i;
            }
        }
        return -1;
    }

    public static String getSuffixName(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."));
    }

    public static float[] getFeatureVectorArray(String featureVector) {
        String[] f = featureVector.split(",");
        float[] ret = new float[f.length];
        IntStream.range(0, f.length).forEachOrdered(i -> ret[i] = Float.parseFloat(f[i]));
        return ret;
    }

    public static String getFeatureVectorString(float[] featureVector) {
        StringJoiner joiner = new StringJoiner(",");
        for (float f : featureVector) {
            joiner.add(String.valueOf(f));
        }
        return joiner.toString();
    }

    @SuppressWarnings("unchecked")
    public static <R> R[] split(String str, Function<String, R> cvt, String deli) {
        String[] ss = str.split(deli);
        return Stream.of(ss).map(cvt).collect(Collectors.toList())
                .toArray((R[]) (Array.newInstance(Object.class, ss.length)));
    }

    /**
     * 字符串切割
     *
     * @param str  要切割的字符串
     * @param deli 隔断符号
     * @return
     */
    public static Integer[] split(String str, String deli) {
        String[] ss = str.split(deli);
        return Stream.of(ss).map(Integer::parseInt).collect(Collectors.toList()).toArray(new Integer[ss.length]);
    }

    /**
     * 拼接
     *
     * @param arr  拼接数组
     * @param deli 隔断符号
     * @param <T>
     * @return
     */
    public static <T> String join(T[] arr, String deli) {
        return Stream.of(arr).map(Object::toString).collect(Collectors.joining(deli));
    }

    /**
     * Base64加密
     *
     * @param str 加密字符串
     * @return
     */
    public static String encodeBase64(String str) {
        byte[] encodedBytes = Base64.getEncoder().encode(str.getBytes());
        return new String(encodedBytes);
    }

    /**
     * Base64解密
     *
     * @param str 解密字符串
     * @return
     */
    public static String decodeBase64(String str) {
        byte[] decodedBytes = Base64.getDecoder().decode(str.getBytes());
        return new String(decodedBytes);
    }

    public static boolean isNum(int chr) {
        return chr >= 48 && chr <= 57;
    }

    public static boolean isLetter(int chr) {
        return (chr >= 65 && chr <= 90) || (chr >= 97 && chr <= 122);
    }


    /**
     * 是否是Ajax异步请求
     *
     * @param request
     */
    public static boolean isAjaxRequest(HttpServletRequest request) {

        String accept = request.getHeader("accept");
        if (accept != null && accept.contains("application/json")) {
            return true;
        }

        String xRequestedWith = request.getHeader("X-Requested-With");
        if (xRequestedWith != null && xRequestedWith.contains("XMLHttpRequest")) {
            return true;
        }

        String uri = request.getRequestURI();
        if (StringUtils.inStringIgnoreCase(uri, ".json", ".xml")) {
            return true;
        }

        String ajax = request.getParameter("__ajax");
        if (StringUtils.inStringIgnoreCase(ajax, "json", "xml")) {
            return true;
        }
        return false;
    }

    /**
     * 是否为静态资源的请求
     *
     * @param request
     * @return
     */
    public static boolean isStaticResources(HttpServletRequest request) {
        String uri = request.getRequestURI();
        return uri.startsWith("/img") || uri.startsWith("/downloadImage") || uri.startsWith("/profile");
    }
}
