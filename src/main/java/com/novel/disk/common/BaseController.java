package com.novel.disk.common;

import com.novel.disk.entity.SysUser;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;

/**
 * web层通用数据处理
 *
 * @author novel
 * @date 2020/7/13
 */
public class BaseController {

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 响应返回结果
     *
     * @param result 影响结果
     * @return 操作结果
     */
    protected Result toAjax(boolean result) {
        return result ? success() : error();
    }

    /**
     * 响应返回结果
     *
     * @param data 数据
     * @return 操作结果
     */
    protected Result toAjax(Object data) {
        return Result.success(data);
    }

    /**
     * 响应返回结果
     *
     * @param result  影响结果
     * @param success 成功消息
     * @return 操作结果
     */
    protected Result toAjax(boolean result, String success) {
        return result ? success(success) : error();
    }

    /**
     * 响应返回结果
     *
     * @param result  影响结果
     * @param success 成功消息
     * @param error   失败结果
     * @return 操作结果
     */
    protected Result toAjax(boolean result, String success, String error) {
        return result ? success(success) : error(error);
    }

    /**
     * 返回成功
     */
    protected Result success() {
        return Result.success();
    }

    /**
     * 返回失败消息
     */
    protected Result error() {
        return Result.error();
    }

    /**
     * 返回成功消息
     */
    protected Result success(String message) {
        return Result.success(message);
    }

    /**
     * 返回失败消息
     */
    protected Result error(String message) {
        return Result.error(message);
    }

    /**
     * 返回错误码消息
     */
    protected Result error(int code, String message) {
        return Result.error(message, code);
    }

    /**
     * 获取当前用户
     *
     * @return 用户
     */
    protected SysUser getUser() {
        Object user = ServletUtils.getRequest().getAttribute("user");
        if (user instanceof SysUser) {
            return (SysUser) user;
        }
        return null;
    }

    /**
     * 获取当前用户id
     *
     * @return 用户id
     */
    protected Long getUserId() {
        if (getUser() != null) {
            return getUser().getId();
        }
        return null;
    }

    /**
     * 获取当前用户用户名
     *
     * @return 用户名
     */
    protected String getUserName() {
        if (getUser() != null) {
            return getUser().getUserName();
        }
        return null;
    }
}
