package com.novel.disk.common;

/**
 * 配置常量
 *
 * @author novel
 * @date 2019/6/21
 */
public interface Constants {


    /**
     * 资源地址
     */
    String FILE_PREFIX = "/download";
    /**
     * 静态资源控制器地址
     */
    String FILE_CONTL = "/file";

    /**
     * 当前记录起始索引
     */
    String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    String IS_ASC = "isAsc";

    /**
     * 系统主机域名key
     */
    String DOMAIN_NAME = "domain_name";
}
