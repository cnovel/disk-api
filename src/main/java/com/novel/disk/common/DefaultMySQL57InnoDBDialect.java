package com.novel.disk.common;

import org.hibernate.dialect.MySQL57InnoDBDialect;

/**
 * @author novel
 * @date 2020/7/13
 */
public class DefaultMySQL57InnoDBDialect extends MySQL57InnoDBDialect {
    @Override
    public String getTableTypeString() {
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8";
    }
}
