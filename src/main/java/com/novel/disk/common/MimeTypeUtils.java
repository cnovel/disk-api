package com.novel.disk.common;

import java.util.Arrays;

/**
 * 媒体类型工具类
 *
 * @author novel
 * @date 2019/5/24
 */
public class MimeTypeUtils extends org.springframework.util.MimeTypeUtils {
    public static final String IMAGE_PNG = "image/png";

    public static final String IMAGE_JPG = "image/jpg";

    public static final String IMAGE_JPEG = "image/jpeg";

    public static final String IMAGE_BMP = "image/bmp";

    public static final String IMAGE_GIF = "image/gif";
    /**
     * 图片
     */
    public static final String[] IMAGE_EXTENSION = {"bmp", "gif", "jpg", "jpeg", "png", "svg"};
    /**
     * 文档
     */
    public static final String[] DOC_EXTENSION = {"doc", "docx", "xls", "xlsx", "ppt", "pptx", "html", "htm", "txt", "pdf", "rtf", "js", "sql", "json", "css", "csv", "chm", "md","yml","xml"};
    /**
     * 视频
     */
    public static final String[] VIDEO_EXTENSION = {"avi", "mp4", "swf", "flv", "wmv", "mpg", "asf", "rm", "rmvb", "mpeg", "mpeg4", "3gp", "mov", "qt", "hlv", "mkv"};
    /**
     * 音乐
     */
    public static final String[] MUSIC_EXTENSION = {"mp3", "wav", "wma", "aac", "ac3", "acc", "ape", "flac", "m4a", "ogg", "oga"};

    public static final String[] FLASH_EXTENSION = {"swf", "flv"};

    public static final String[] MEDIA_EXTENSION = {"swf", "flv", "mp3", "wav", "wma", "wmv", "mid", "avi", "mpg", "asf", "rm", "rmvb"};

    public static final String[] DEFAULT_ALLOWED_EXTENSION = {
            // 图片
            "bmp", "gif", "jpg", "jpeg", "png", "svg",
            // word excel powerpoint
            "doc", "docx", "xls", "xlsx", "ppt", "pptx", "html", "htm", "txt",
            // 压缩文件
            "rar", "zip", "gz", "bz2",
            // pdf
            "pdf"};

    public static String getExtension(String prefix) {
        switch (prefix) {
            case IMAGE_PNG:
                return "png";
            case IMAGE_JPG:
                return "jpg";
            case IMAGE_JPEG:
                return "jpeg";
            case IMAGE_BMP:
                return "bmp";
            case IMAGE_GIF:
                return "gif";
            default:
                return "";
        }
    }

    /**
     * 获取文件类型
     *
     * @param fileSuffix 扩展名
     * @return 类型
     */
    public static Integer getFileType(String fileSuffix) {
        if (Arrays.asList(IMAGE_EXTENSION).contains(fileSuffix)) {
            return 1;
        } else if (Arrays.asList(DOC_EXTENSION).contains(fileSuffix)) {
            return 2;
        } else if (Arrays.asList(VIDEO_EXTENSION).contains(fileSuffix)) {
            return 3;
        } else if (Arrays.asList(MUSIC_EXTENSION).contains(fileSuffix)) {
            return 4;
        } else {
            return 5;
        }
    }
}
