package com.novel.disk.controller;

import com.novel.disk.common.BaseController;
import com.novel.disk.common.Result;
import com.novel.disk.entity.StorageEntity;
import com.novel.disk.framework.config.Resource;
import com.novel.disk.service.StorageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 存储服务 接口
 *
 * @author 李振
 * @date 2020/7/29
 */
@RestController
@RequestMapping("/storage")
public class StorageController extends BaseController {
    private final StorageService storageService;

    public StorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping
    public Result getStorageByUserId() {
        StorageEntity storageEntity = storageService.getStorageByUserId(getUserId());
        if (storageEntity == null) {
            storageEntity = new StorageEntity();
            storageEntity.setTotalSize(Resource.getStorageTotalSize());
            storageEntity.setUsedSize(0L);
            storageEntity.setUserId(getUserId());
            storageService.saveStorage(storageEntity);
        }
        return toAjax(storageEntity);
    }
}
