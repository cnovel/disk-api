package com.novel.disk.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.File;
import java.util.Date;
import java.util.List;


/**
 * 文件
 *
 * @author novel
 * @date 2020/7/13
 */
@Getter
@Setter
@Entity
@Table(name = "file", uniqueConstraints = {
        @UniqueConstraint(columnNames = "fileName"),
        @UniqueConstraint(columnNames = "filePath")
})
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@EntityListeners(AuditingEntityListener.class)
public class FileEntity {
    /**
     * 文件id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fileId;

    /**
     * 父级目录
     */
    @JoinColumn(name = "pid", referencedColumnName = "pid")
    private Long pid;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 文件名
     */
    private String fileName;
    /**
     * 扩展名
     */
    private String extendName;
    /**
     * 文件路径
     */
    private String filePath;
    /**
     * 文件大小
     */
    private Long fileSize;
    /**
     * 是否是目录
     */
    private Boolean isDir;

    /**
     * hash值
     */
    private String hash;

    /**
     * 文件类型
     */
    private Integer type;

    /**
     * 文件下载次数
     */
    private Integer downloadNum;

    /**
     * 文件分类
     */
    private String typeName;

    /**
     * 创建时间
     */
    @CreatedDate
    private Date createTime;

    /**
     * 最后修改时间
     */
    @LastModifiedDate
    private Date lastModifyTime;

    @Transient
    private String url;

    @JsonIgnore
    @ManyToOne(targetEntity = FileEntity.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "pid", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT), insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private FileEntity pFileEntity;

    @JsonIgnore
    @OneToMany(targetEntity = FileEntity.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "pid", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    private List<FileEntity> fileEntityList;


    /**
     * 文件内容
     */
    @JsonIgnore
    @Transient
    private File file;

    public String getName() {
        if (fileName.contains("/")) {
            return fileName.substring(fileName.lastIndexOf("/") + 1);
        } else {
            return fileName;
        }
    }

    public String getPath() {
        if (filePath.contains("/")) {
            return filePath.substring(filePath.lastIndexOf("/") + 1);
        } else {
            return filePath;
        }
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
        if (filePath != null) {
            this.filePath = this.filePath.replaceAll("\\\\", "/");
            this.filePath = this.filePath.replaceAll("//", "/");
        }
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
        if (fileName != null) {
            this.fileName = this.fileName.replaceAll("\\\\", "/");
            this.fileName = this.fileName.replaceAll("//", "/");
        }
    }

    @Override
    public String toString() {
        return "FileEntity{" +
                "fileId=" + fileId +
                ", pid=" + pid +
                ", userId=" + userId +
                ", fileName='" + fileName + '\'' +
                ", extendName='" + extendName + '\'' +
                ", filePath='" + filePath + '\'' +
                ", fileSize=" + fileSize +
                ", isDir=" + isDir +
                ", hash='" + hash + '\'' +
                ", type=" + type +
                ", downloadNum=" + downloadNum +
                ", typeName='" + typeName + '\'' +
                ", createTime=" + createTime +
                ", lastModifyTime=" + lastModifyTime +
                '}';
    }
}
