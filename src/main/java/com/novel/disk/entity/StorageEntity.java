package com.novel.disk.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 存储信息类
 *
 * @author novel
 * @date 2020/7/29
 */
@Table(name = "storage")
@Entity
@Getter
@Setter
public class StorageEntity {
    /**
     * 存储id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long storageId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 已使用存储大小
     */
    @Column(columnDefinition = "bigint default 0")
    private Long usedSize;

    /**
     * 总存储大小
     */
    @Column(columnDefinition = "bigint default 0")
    private Long totalSize;

    public Long getStorageId() {
        return storageId;
    }

    public void setStorageId(Long storageId) {
        this.storageId = storageId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Long getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(Long usedSize) {
        if (usedSize < 0) {
            usedSize = 0L;
        }
        this.usedSize = usedSize;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        if (totalSize < 0) {
            totalSize = 0L;
        }
        this.totalSize = totalSize;
    }
}
