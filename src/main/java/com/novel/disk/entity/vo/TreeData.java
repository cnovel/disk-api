package com.novel.disk.entity.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 树组件对应结构体
 *
 * @author novel
 * @date 2020/7/22
 */
@Data
@ToString
public class TreeData implements Serializable {
    /**
     * id
     */
    private Long id;
    /**
     * 名称
     */
    private String label;
    /**
     * 子节点
     */
    private List<TreeData> children;
}
