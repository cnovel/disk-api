package com.novel.disk.framework.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 项目配置信息
 *
 * @author novel
 * @date 2020/7/24
 */
@Data
@ConfigurationProperties(prefix = "project.authorization")
@Configuration
public class LoginProperties {
    /**
     * 认证服务器地址
     */
    private String url;
}
