package com.novel.disk.framework.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

/**
 * 项目基本配置
 *
 * @author novel
 * @date 2020/7/28
 */
@Data
@ConfigurationProperties(prefix = "project")
@Configuration
public class ProjectProperties {
    /**
     * 系统详细信息
     */
    @NestedConfigurationProperty
    private ProjectInfo projectInfo = new ProjectInfo();
    /**
     * 系统资源信息
     */
    @NestedConfigurationProperty
    private Resource resource = new Resource();
}
