package com.novel.disk.framework.config;

import lombok.Data;

/**
 * 资源信息配置
 *
 * @author novel
 * @date 2020/8/5
 */
@Data
public class Resource {

    /**
     * 文件存放根目录
     */
    private String rootPath;
    /**
     * 用户存储空间总大小（单位：b）
     */
    private static Long storageTotalSize = 10737418240L;

    public static Long getStorageTotalSize() {
        return storageTotalSize;
    }

    public void setStorageTotalSize(Long storageTotalSize) {
        Resource.storageTotalSize = storageTotalSize;
    }
}
