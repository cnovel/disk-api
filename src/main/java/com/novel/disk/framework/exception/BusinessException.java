package com.novel.disk.framework.exception;


import com.novel.disk.framework.exception.base.SuperException;

/**
 * 业务异常
 *
 * @author novel
 * @date 2018/7/31
 */
public class BusinessException extends SuperException {
    private static final long serialVersionUID = 1L;

    public BusinessException(String message, Integer code) {
        super(message, code);
    }
}
