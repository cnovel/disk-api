package com.novel.disk.framework.exception;

import com.novel.disk.framework.exception.base.SuperException;

/**
 * 登录超时
 *
 * @author novel
 * @date 2020/7/24
 */
public class LoginTimeoutException extends SuperException {
    public LoginTimeoutException(String message, Integer code) {
        super(message, code);
    }
}
