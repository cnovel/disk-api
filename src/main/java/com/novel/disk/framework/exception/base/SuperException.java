package com.novel.disk.framework.exception.base;

/**
 * 项目异常父类
 *
 * @author novel
 * @date 2018/7/28
 */
public class SuperException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private Integer code;

	public SuperException(String message, Integer code) {
		super(message);
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
