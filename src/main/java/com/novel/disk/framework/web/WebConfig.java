package com.novel.disk.framework.web;

import com.novel.disk.framework.config.LoginProperties;
import com.novel.disk.framework.Interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 配置类
 *
 * @author novel
 * @date 2020/7/24
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    private final RestTemplate restTemplate;
    private final LoginProperties loginProperties;

    public WebConfig(RestTemplate restTemplate, LoginProperties loginProperties) {
        this.restTemplate = restTemplate;
        this.loginProperties = loginProperties;
    }

    /**
     * 注册拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //addPathPattern后跟拦截地址，excludePathPatterns后跟排除拦截地址
        registry.addInterceptor(new LoginInterceptor(restTemplate, loginProperties)).addPathPatterns("/**").excludePathPatterns("/file/download*","/file/preview*");
    }
}
