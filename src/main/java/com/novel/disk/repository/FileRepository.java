package com.novel.disk.repository;

import com.novel.disk.entity.FileEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 文件
 *
 * @author novel
 * @date 2020/7/13
 */
@Repository
public interface FileRepository extends JpaRepository<FileEntity, Long>, JpaSpecificationExecutor<FileEntity> {

    /**
     * 根据父级id查询
     *
     * @param pid  父级id
     * @param sort 排序
     * @return 结果
     */
    List<FileEntity> findByPid(Long pid, Sort sort);


    /**
     * 条件查询
     *
     * @param pid      父级目录id
     * @param userId   用户id
     * @param type     文件类型
     * @param pPath    父级文件路径
     * @param fileName 文件路径
     * @return 结果
     */
    @Query(value = "SELECT f.* FROM file f WHERE IF (?1 is not null,f.pid = ?1,1=1) AND IF (?2 !='',f.user_id = ?2,1=1) AND IF (?3 !=0,f.type = ?3,1=1)  AND IF (?5 !='',f.file_name LIKE concat(?4,'%',?5,'%'),1=1) ORDER BY is_dir DESC", nativeQuery = true)
    List<FileEntity> findAll(Long pid, Long userId, Integer type, String pPath, String fileName);

    /**
     * 删除所有以指定filePath开头的数据
     *
     * @param filePath 路径
     */
    void deleteAllByFilePathIsStartingWith(String filePath);

    /**
     * 查询根据filepath开头的数据
     *
     * @param filePath 文件路径
     * @return 结果
     */
    List<FileEntity> findAllByFilePathIsStartingWith(String filePath);

    /**
     * 删除所有以指定fileName开头的数据
     *
     * @param fileName 文件名
     */
    void deleteAllByFileNameIsStartingWith(String fileName);

    /**
     * 根据文件名查询
     *
     * @param fileName 文件名
     * @return 文件信息
     */
    FileEntity findByFileName(String fileName);
}
