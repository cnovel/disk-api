package com.novel.disk.repository;

import com.novel.disk.entity.StorageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * 存储
 *
 * @author novel
 * @date 2020/7/29
 */
@Repository
public interface StorageRepository extends JpaRepository<StorageEntity, Long>, JpaSpecificationExecutor<StorageEntity> {
}
