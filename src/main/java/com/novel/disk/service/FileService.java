package com.novel.disk.service;

import com.novel.disk.entity.FileEntity;
import com.novel.disk.entity.vo.TreeData;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

/**
 * 文件服务
 *
 * @author novel
 * @date 2020/7/13
 */
public interface FileService {

    /**
     * 新建目录
     *
     * @param dirName 目录名称
     * @param pid     当前目录的id
     * @param userId  当前用户的id
     * @return 结果
     */
    boolean mkdir(String dirName, Long pid, Long userId);

    /**
     * 列出某个目录下所有文件和文件夹
     *
     * @param fileEntity 查询条件
     * @return 结果
     */
    List<FileEntity> listDir(FileEntity fileEntity);

    /**
     * 上传文件
     *
     * @param file   文件流
     * @param pid    目录
     * @param userId 当前用户的id
     * @return 结果
     */
    boolean upLoad(MultipartFile file, Long pid, Long userId);

    /**
     * 删除指定资源
     *
     * @param sourceListId 资源id集合
     * @param userId       当前用户的id
     * @return 结果
     */
    boolean remove(Long[] sourceListId, Long userId);

    /**
     * 重命名
     *
     * @param id     资源id
     * @param name   资源名称
     * @param userId 当前用户的id
     * @return 结果
     */
    boolean rename(Long id, String name, Long userId);

    /**
     * 资源复制
     *
     * @param sourceListId 资源id集合
     * @param targetId     目标目录
     * @param userId       当前用户的id
     * @return 结果
     */
    boolean copyDir(Long[] sourceListId, Long targetId, Long userId);

    /**
     * 资源移动
     *
     * @param sourceListId 资源id集合
     * @param targetId     目标目录
     * @param userId       当前用户的id
     * @return 结果
     */
    boolean moveDir(Long[] sourceListId, Long targetId, Long userId);


    /**
     * 下载文件
     *
     * @param fileId 文件id
     * @return 结果
     */
    byte[] download(Long fileId);

    /**
     * 读取文件信息
     *
     * @param path 文件路径
     * @return 结果
     */
    File readBytes(String path);

    /**
     * 返回树形目录结构
     *
     * @param userId 当前用户的id
     * @return 结果
     */
    TreeData listDirTree(Long userId);

    /**
     * 根据文件id获取文件
     *
     * @param fileId 文件id
     * @return 结果
     */
    FileEntity getFileById(Long fileId);

    /**
     * 获取下载链接以及文件信息
     *
     * @param fileId 文件id
     * @return 结果
     */
    FileEntity getDownloadUrl(Long fileId);

    /**
     * 解压
     *
     * @param fileId 文件id
     * @param userId 用户id
     * @return 结果
     */
    boolean unzip(Long fileId, Long userId);

    /**
     * 文件压缩
     *
     * @param fileIds 文件id
     * @param userId  用户id
     * @return 结果
     */
    boolean zip(Long[] fileIds, Long userId);
}
