package com.novel.disk.service;

import com.novel.disk.entity.FileEntity;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * 资源服务接口
 *
 * @author novel
 * @date 2020/7/13
 */
public interface ResourceService {
    /**
     * 新建目录
     *
     * @param path 路径
     * @return 结果
     */
    boolean mkdir(String path);

    /**
     * 列出某个目录下所有文件和文件夹
     *
     * @param path 路径
     * @return 结果
     */
    List<File> listDir(String path);

    /**
     * 文件上传
     *
     * @param inputStream 文件流
     * @param targetPath  目标路径
     * @return 结果
     */
    boolean upLoad(InputStream inputStream, String targetPath);

    /**
     * 删除指定资源
     *
     * @param path 资源路径
     * @return 结果
     */
    boolean remove(String path);

    /**
     * 删除指定资源
     *
     * @param file 资源
     * @return 结果
     */
    boolean remove(File file);

    /**
     * 重命名
     *
     * @param source 原资源名称
     * @param target 新资源名称
     * @return 结果
     */
    boolean rename(String source, String target);

    /**
     * 重命名
     *
     * @param file   原资源
     * @param target 新资源名称
     * @return 结果
     */
    boolean rename(File file, String target);

    /**
     * 资源复制
     *
     * @param source 原资源路径
     * @param target 新资源路径
     * @return 结果
     */
    boolean copyDir(String source, String target);

    /**
     * 资源移动
     *
     * @param source 原资源路径
     * @param target 新资源路径
     * @return 结果
     */
    boolean moveDir(String source, String target);

    /**
     * 移动文件到指定目录
     *
     * @param source 原资源路径
     * @param target 新资源路径
     * @return 结果
     */
    boolean moveDir(File source, String target);

    /**
     * 读取文件内容
     *
     * @param filePath 文件路径
     * @return 结果
     */
    File readBytes(String filePath);

    /**
     * 下载
     *
     * @param fileEntity 文件信息
     * @return 结果
     */
    byte[] download(FileEntity fileEntity);

    /**
     * 压缩文件夹
     *
     * @param fileEntity 文件信息
     * @return 结果
     */
    String zip(FileEntity fileEntity);

    /**
     * 压缩文件
     *
     * @param fileEntityList 文件列表信息
     * @return 结果
     */
    File zip(List<FileEntity> fileEntityList);

    /**
     * 解压
     *
     * @param fileEntity 压缩文件
     * @return 结果
     */
    File unzip(FileEntity fileEntity);
}
