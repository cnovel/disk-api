package com.novel.disk.service;

import com.novel.disk.entity.StorageEntity;

/**
 * 存储服务
 *
 * @author novel
 * @date 2020/7/13
 */
public interface StorageService {

    /**
     * 根据用户id查询存储信息
     *
     * @param userId 用户id
     * @return 储存信息
     */
    StorageEntity getStorageByUserId(Long userId);

    /**
     * 保存
     *
     * @param storageEntity 储存信息
     * @return 结构
     */
    boolean saveStorage(StorageEntity storageEntity);

    /**
     * 更新
     *
     * @param storageEntity 储存信息
     * @return 结果
     */
    boolean updateStorage(StorageEntity storageEntity);
}
