package com.novel.disk.service.impl;

import com.novel.disk.entity.StorageEntity;
import com.novel.disk.repository.StorageRepository;
import com.novel.disk.service.StorageService;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 存储服务 实现
 *
 * @author novel
 * @date 2020/7/29
 */
@Service
public class StorageServiceImpl implements StorageService {
    private final StorageRepository storageRepository;

    public StorageServiceImpl(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }


    @Override
    public StorageEntity getStorageByUserId(Long userId) {
        StorageEntity storageEntity = new StorageEntity();
        storageEntity.setUserId(userId);
        List<StorageEntity> list = storageRepository.findAll(Example.of(storageEntity));
        return list.size() > 0 ? list.get(0) : null;
    }

    @Override
    public boolean saveStorage(StorageEntity storageEntity) {
        storageRepository.save(storageEntity);
        return true;
    }

    @Override
    public boolean updateStorage(StorageEntity storageEntity) {
        storageRepository.save(storageEntity);
        return true;
    }
}
