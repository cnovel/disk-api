package com.novel.disk;

import com.novel.disk.entity.FileEntity;
import com.novel.disk.repository.FileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
class DiskApplicationTests {

    @Autowired
    private FileRepository fileRepository;

    @Test
    void contextLoads() {
        FileEntity fileEntity = new FileEntity();
        fileEntity.setCreateTime(new Date());
        fileEntity.setLastModifyTime(new Date());
        fileEntity.setDownloadNum(2);
        fileEntity.setExtendName("jpg");
        fileEntity.setFilePath("/test.jpg");
        fileEntity.setFileName("test.jpg");
        fileEntity.setFileSize(5000L);
        fileEntity.setHash("sdsdggfhgfsghthtrsgsegdf");
        fileEntity.setPid(0L);
        fileEntity.setIsDir(false);
        fileEntity.setType(1);
        fileRepository.save(fileEntity);
    }

}
