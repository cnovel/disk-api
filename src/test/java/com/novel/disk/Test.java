package com.novel.disk;

import cn.hutool.core.io.file.FileNameUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author novel
 * @date 2020/7/13
 */
public class Test {
    public static void main(String[] args) throws Exception {
//        InputStream is = null;
//        try {
////             is = new FileInputStream();
//            byte[] file = FileUtils.getBytesByFile(new File("E:\\tmp\\test\\1e8a040fd64ef9c6219c525bfaa74ed4.jpg"));
//            System.out.println("len: " + file.length);
//        } catch (Exception e) {
//        } finally {
//            if (is != null) is.close();
//        }

        new Test().testFileName();
    }

    private void verifyFileName() {
        String fileName = "E:\\tmp\\1\\e4f0836d8a98efc17c1b82aee69f09f8(1).zip";

        String name = FileNameUtil.mainName(fileName);
        Pattern p = Pattern.compile("\\((\\d+)\\)$");
//        Pattern p = Pattern.compile("/.*\\((.*)\\)/");
        Matcher m = p.matcher(name);
        while (m.find()) {
            String group = m.group(0);
            System.out.println(group);
        }

    }

    private void testFileName() {
        String fileName = "E:\\tmp\\1\\e4f0836d8a98efc17c1b82aee69f09f8(1).zip";

        System.out.println(FileNameUtil.getPrefix(fileName));
        System.out.println(FileNameUtil.getName(fileName));
        System.out.println(FileNameUtil.getSuffix(fileName));

    }
}
