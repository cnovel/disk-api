package com.novel.disk;

import com.github.junrar.Archive;
import com.github.junrar.Junrar;
import com.github.junrar.rarfile.FileHeader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * rar 压缩解压测试
 *
 * @author 李振
 * @date 2021/4/19 10:59
 */
public class Test2 {
    public static void main(String[] args) {
        String originDir = "D:\\JavaPro\\TestFile\\";
        String rarPath = originDir + "new.rar";
        File rarFile = new File(rarPath);

        try {
//            unRar(rarFile, "D:\\JavaPro\\TestFile\\测试\\");
            Junrar.extract("F:\\迅雷下载\\diskgenius(jb51.net).rar", "F:\\迅雷下载\\");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void unRar(File rarFile, String outDir) throws Exception {
        File outFileDir = new File(outDir);
        if (!outFileDir.exists()) {
            boolean isMakDir = outFileDir.mkdirs();
            if (isMakDir) {
                System.out.println("创建压缩目录成功");
            }
        }
        Archive archive = new Archive(new FileInputStream(rarFile));
        FileHeader fileHeader = archive.nextFileHeader();
        while (fileHeader != null) {
            if (fileHeader.isDirectory()) {
                fileHeader = archive.nextFileHeader();
                continue;
            }
            File out = new File(outDir + fileHeader.getFileName());
            if (!out.exists()) {
                if (!out.getParentFile().exists()) {
                    out.getParentFile().mkdirs();
                }
                out.createNewFile();
            }
            FileOutputStream os = new FileOutputStream(out);
            archive.extractFile(fileHeader, os);

            os.close();

            fileHeader = archive.nextFileHeader();
        }
        archive.close();
    }
}
